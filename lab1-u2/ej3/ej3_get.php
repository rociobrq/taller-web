<!DOCTYPE html>
<html class="no-js" lang="es">
  <head>
    <meta charset="utf-8">
    <!-- Se carga la hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">
    <!-- Título de la pestaña -->
    <title>Ejercicio 3 - GET</title>
  </head>
  <body>
    <p id="titulo">TABLA TAMAÑO NXN</p>

    <!-- Generación del formulario con método GET -->
    <center>
      <h2>Método GET</h2>
      <!-- Actualiza la misma página -->
      <form method="get" action="ej3_get.php">
        <!-- Generación de tabla del formulario -->
        <table>
          <tr>
            <!-- Input del número para generar tabla NXN -->
            <td align="left">Número:</td>
            <td align="right" colspan="3"><input type="text" name="numero" size="25"></input></td>
          </tr>
          <tr>
            <!-- Input del color 1 -->
            <td align="left">Color 1:</td>
            <!-- Color picker con un color por defecto -->
            <td align="right" colspan="3"><input type="color" value="#b3ffff" name="color1" size="25"></td>
          </tr>
          <tr>
            <!-- Input del color 2 -->
            <td align="left">Color 2:</td>
            <!-- Color picker con un color por defecto -->
            <td align="right" colspan="3"><input type="color" value="#e6b3ff" name="color2" size="5"></td>
          </tr>
        </table><br><br>
        <!-- Botones para enviar valores o resetear -->
        <input type="submit" value = "Generar"> <input type="reset"><br><br>
      </form>
    </center>

    <?php
      // Se recuperan inputs del formulario
      if ($_SERVER["REQUEST_METHOD"] == "GET"){
        // Verificación del número
        if (isset($_GET["numero"])) {
          $num = $_GET["numero"];

          // Verificación de colores
          if (isset($_GET["color1"])){
            $color1  = $_GET["color1"];
          }

          if (isset($_GET["color2"])){
            $color2  = $_GET["color2"];
          }
        }
      }
    ?>

    <!-- Definición de tabla NXN-->
    <table id="tabla-ej">
    <!-- Código php -->
      <?php
        $contador = 0;

        // Ciclo para filas
        for ($x = 1;$x <= $num; $x++){
          // Definición de celdas por fila
          echo ("<tr>");
          // Ciclo para columnas
          for ($y = 1;$y <= $num; $y++){
            $contador = $contador + 1;

            if($x%2 == 0){
              // Celdas pares se colorean con color1
              echo("<td id='td-ej' bgcolor=$color1>");
            }
            else{
              // Celdas impares se colorean con color2
              echo("<td id='td-ej' bgcolor=$color2>");
            }

            // Se imprime valor en celda
            echo ("$contador");
            echo("</td>");
          }
          echo ("</tr>");
        }
      ?>
    </table>
  </body>
</html>
