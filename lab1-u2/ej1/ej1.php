<!DOCTYPE html>
<html class="no-js" lang="es">
  <head>
    <!-- Se carga la hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">
    <!-- Título de la pestaña -->
    <title>Ejercicio 1</title>
  </head>
  <body>
    <p id="titulo">TABLA DEL 1 AL 100</p>

    <!-- Generación de tabla -->
    <table id="tabla-ej">
    <!-- Código php -->
      <?php
        // Se genera contador para recorrer celdas
        $contador = 0;
        // Ciclo para generar filas
        for ($x = 1;$x <= 10; $x++ ) {
          // Etiqueta html para definir celdas de la fila
          echo ("<tr>");
          // Ciclo para generar columnas
          for ($y = 1;$y <= 10; $y++ ) {
            $contador = $contador + 1;
            echo ("<td id='td-ej'>");
            // Se imprime valor en celda
            echo ("$contador");
            echo ("</td>");
          }
          echo ("</tr>");
        }
      ?>
    </table>
  </body>
</html>
