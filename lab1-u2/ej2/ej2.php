<!DOCTYPE html>
<html class="no-js" lang="es">
  <head>
    <!-- Se carga la hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">
    <!-- Título de la pestaña -->
    <title>Ejercicio 2</title>
  </head>
  <body>
    <p id="titulo">TABLA NXN GENERADA CON DEFINE</p>

    <!-- Generación de tabla -->
    <table id="tabla-ej">
    <!-- Código php -->
      <?php
        // Valor constante
        define("TAM", 9);

        $contador = 0;
        // Ciclo para filas
        for ($x = 1;$x <= TAM; $x++ ) {
          // Define celdas para cada fila
          echo ("<tr>");
          // Ciclo para columnas
          for ($y = 1;$y <= TAM; $y++ ) {
            $contador = $contador + 1;
            if($x%2 == 0){
              // Filas pares se colorean de gris
              echo("<td id='td-ej' bgcolor=gray>");
            }
            else{
              // Filas impares se colorean de blanco
              echo("<td id='td-ej' bgcolor=white>");
            }
            // Se imprime valor en celda
            echo ("$contador");
            echo ("</td>");
          }
          echo ("</tr>");
        }
      ?>
    </table>
  </body>
</html>
