// Se obtienen inputs con prompt.
var firstNum = prompt("Ingresa el primer número: ").trim();
var secondNum = prompt("Ingresa el segundo número: ").trim();
var thirdNum = prompt("Ingresa el tercer número: ").trim();
// Se guardan inputs en arreglo.
var numbers = [firstNum, secondNum, thirdNum];


// Validación de números.
if (validate(numbers)) {
  // Llama a la función para encontrar el mayor número.
  compare(numbers);
}

// Valida inputs.
function validate(numbers) {
  // Función flecha que evalúa si un elemento tiene contenido.
  let checker_empty = (arr) => arr !== "";

  // Se evalúan todos los elementos del arreglo en la función flecha.
  // Condicional se cumple si todos los elementos tienen contenido.
  if (numbers.every(checker_empty)) {
    // Función flecha que evalúa si un elemento no es número.
    let checker_NaN = (arr) => isNaN(Number(arr));

    // Condicional se cumple si alguno de los elementos no es de tipo numérico.
    if (numbers.some(checker_NaN)) {
      alert("¡Error! No se puede realizar la operación.");
    } else {
      // Se retorna true si todos los elementos ingresados son numéricos.
      return true;
    }
  } else {
    alert("¡Error! No se puede realizar la operación.");
  }
}

// Encuentra el número mayor de 3 ingresados.
function compare(numbers) {
  var text = "Números ingresados: " + numbers + "\n";
  // Compara si todos son iguales.
  if (numbers[0] == numbers[1] && numbers[0] == numbers[1]) {
    alert(text + "Los números son iguales.");
  } else {
    // Spread operator.
    let max_number = Math.max(...numbers.map(Number));
    alert(text + "El número mayor es: " + max_number);
  }
}
