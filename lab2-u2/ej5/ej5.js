// Se recibe el input con prompt.
var input_number = prompt("Ingresa un número: ").trim();

// Se llama a la función para que valide si el input es número.
if (validate(input_number)) {
  if (input_number % 2 == 0) {
    alert("El valor es divisible por 2.");
  } else {
    alert("El valor no es divisible por 2.");
  }
}

function validate(input_number) {
  // Se chequea que el input no sea igual en valor ni en tipo.
  if (input_number !== "") {
    // Se revisa que se haya ingresado un valor numérico.
    if (isNaN(Number(input_number))) {
      alert("¡Error! Se ha ingresado un valor no numérico.");
    } else {
      return true;
    }
  } else {
    alert("¡Error! No se ha ingresado un valor.")
  }
}
