// Se recibe input con prompt.
var text = prompt("Ingresa una frase: ");

// Se llama a la función validate().
if (validate(text)) {
  // Contador para letra a.
  var count = 0;
  // Ciclo recorre el string.
  for (i = 0; i < text.length; i++) {
    // Actualiza contador si se encuentra la letra a.
    if (text[i] == "a") {
      count++;
    }
  }

  // Se escribe el resultado final.
  let alert_text = "Frase ingresada: " + "'" + text + "'" + "\n";
  alert(alert_text + "Frecuencia de letra 'a': " + count);
}

// Valida que el input presente contenido.
function validate(text) {
  // Validaciónde string verificando que contenga letras.
  // Verifica que un string no tenga espacios.
  let check_space = text.trim();
  // Se verifica que el input no esté vacío.
  if (text === "" || check_space.length == 0) {
    alert("No se ha ingresado nada.");
  } else {
    return true;
  }
}
