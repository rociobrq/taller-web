function play() {
  // Se obtiene random.
  let get_random = random();

  // Dependiendo del número, se carga imagen.
  switch (get_random) {
    case 1:
      document.getElementById("current_play").src="./fotos/1.png";
      break;
    case 2:
      document.getElementById("current_play").src="./fotos/2.png";
      break;
    case 3:
      document.getElementById("current_play").src="./fotos/3.png";
      break;
    case 4:
      document.getElementById("current_play").src="./fotos/4.png";
      break;
    case 5:
      document.getElementById("current_play").src="./fotos/5.png";
      break;
    case 6:
      document.getElementById("current_play").src="./fotos/6.png";
      break;
  }
}

function random() {
  let min = 1;
  let max = 6;
  // Retorna un número random entre 1 y 6, ambos inclusive.
  return Math.floor(Math.random() * (max - min + 1) + min)
}
