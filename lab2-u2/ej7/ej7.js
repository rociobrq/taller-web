// Recibe frase.
var text = prompt("Ingresa una frase: ");
// String con vocales.
var vowels = "aeiou";

if (validate(text)) {
  // Input tiene contenido.

  // Se define arreglo para guardar todas las vocales.
  let vowels_print = [];

  // Ciclo for recorre el string.
  for (i = 0; i < text.length; i++) {
    // Se revisa si cada letra está dentro del string de vocales.
    if (vowels.includes(text[i].toLowerCase())) {
      // Se añade vocal al arreglo.
      vowels_print.push(text[i]);
    }
  }

  let alert_text = "Frase ingresada: " + "'" + text + "'" + "\n";

  // Se chequea si el arreglo está vacío.
  if (vowels_print.length == 0) {
    alert(alert_text + "No aparece ninguna vocal.");
  } else {
    alert(alert_text + "Todas las vocales que aparecen: " + vowels_print);
  }
}

function validate(text) {
  // Validaciónde string verificando que contenga letras.
  // String sin espacios.
  let check_space = text.trim();
  // Se verifica que el input no esté vacío.
  if (text === "" || check_space.length == 0) {
    alert("No se ha ingresado nada.");
  } else {
    return true;
  }
}
