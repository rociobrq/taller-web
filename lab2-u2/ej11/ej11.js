// Se define arreglo para imágenes
var img_array = new Array();

// Se añade la ruta de imágenes al arreglo
img_array[0] = './fotos/eva.jpg';
img_array[1] = './fotos/gojo.jpg';
img_array[2] = './fotos/lain.png';
img_array[3] = './fotos/eren.jpeg';
img_array[4] = './fotos/lainddd.jpg';

// Retrocede en el arreglo.
function previous() {
  // Se obtiene el índice actual.
  let index = get_index();

  // Si no es la primera imagen, se puede retroceder.
  if(index > 0){
    // Se le resta uno para retroceder.
    let new_index = index - 1;
    // Se llama a la función que cambia imagen.
    change(new_index);
  } else {
    alert("¡Primera imagen!");
  }
}

// Avanza en el arreglo.
function next() {
  // Obtiene el índice actual.
  let index = get_index();
  // Si imagen no es la última, se puede avanzar.
  if(index < img_array.length - 1){
    // Se le suma uno para avanzar.
    let new_index = index + 1;
    // Se cambia la imagen.
    change(new_index);
  } else {
    alert("No hay más imágenes...");
  }
}

// Cambia la imagen en el contenedor.
function change(new_index) {
  // Se obtiene el índice de la imagen nueva y la ruta guardada en el arreglo.
  new_slide = img_array[new_index]

  // Se actualiza la ruta en el contenedor.
  document.getElementById("current_slide").src= new_slide;
}

// Obtiene el índice actual.
function get_index() {
  let current = document.getElementById('current_slide');
  // Se obtiene el nombre de la foto.
  let source = "./fotos/" + current.src.split("/").pop();
  // Se retorna el índice.
  return img_array.indexOf(source);
}
