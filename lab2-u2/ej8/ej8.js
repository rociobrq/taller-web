// Se recibe input con prompt.
var text = prompt("Ingresa una frase: ");
// String de vocales.
var vowels = "aeiou";

// Diccionario para almacenar vocales y frecuencia de cada una.
var vowels_dict = {};

// Para cada letra, se inicia el contador en 0.
for (i = 0; i < 5; i++) {
  vowels_dict[vowels[i]] = 0;
}


// Se realiza conteo de vocales si string es válido.
if (validate(text)) {
  // Si input tiene contenido, ciclo for recorre el string.
  for (i = 0; i < text.length; i++) {
    // Se revisa si cada letra del string es una vocal.
    if (vowels.includes(text[i])) {
      // Si se encuentra una vocal, se recupera el valor del contador.
      let value = vowels_dict[text[i]];
      // Se actualiza su contador.
      vowels_dict[text[i]] = value + 1;
    }
  }

  //Arreglo para guardar contenido de diccionario.
  let vowels_array = [];
  // Se recorre el diccionario.
  for (const key of Object.keys(vowels_dict)) {
    // Se añaden valores de diccionario al arreglo.
    vowels_array.push("Vocal: " + key + " - Frecuencia: " + vowels_dict[key]);
  }

  // Arreglo se une con un caracter.
  vowels_array = vowels_array.join('\n');
  let alert_text = "Frase ingresada: " + "'" + text + "'" + "\n";
  alert(alert_text + vowels_array);
}

function validate(text) {
  // Validaciónde string verificando que contenga letras.
  // String sin espacios.
  let check_space = text.trim();
  // Se verifica que el input no esté vacío.
  if (text === "" || check_space.length == 0) {
    alert("No se ha ingresado nada.");
  } else {
    return true;
  }
}
