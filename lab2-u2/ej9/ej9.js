// Función convierte de clp a dólares.
function convert_clp() {
  // Obtiene valor de clp.
  var clp = document.getElementById("clp-to-dol").value;

  // Valida ingreso del input en CLP sin espacios.
  if (validate(clp.trim())) {
    // Operación para calcular dólares.
    let dol = clp * 0.0012;
    // Reemplaza el valor de dólares en la etiqueta.
    document.getElementById("dol-from-clp").value = dol;
  }
}

// Función convierte de dólares a clp.
function convert_dol() {
  // Obtiene valor de dólares.
  var dol = document.getElementById("dol-to-clp").value;

  // Valida ingreso del input en dólares sin espacios.
  if (validate(dol.trim())) {
    // Operación para calcular clp.
    let clp = dol * 854.7;
    // Reemplaza el valor de clp en la etiqueta.
    document.getElementById("clp-from-dol").value = clp;
  }
}

function validate(input) {
  // Se evalúa que el input tenga contenido.
  if (input !== "") {
    // Evalúa si un elemento no es número.
    if (isNaN(Number(input))) {
      alert("¡Error! No se puede realizar la operación.");
    } else {
      // Se retorna true si todos los elementos ingresados son numéricos.
      return true;
    }
  } else {
    alert("¡Error! No se puede realizar la operación.");
  }
}
