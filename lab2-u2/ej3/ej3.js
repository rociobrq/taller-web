// Se obtienen números con prompt.
var firstNum = prompt("Ingresa un número: ").trim();
var secondNum = prompt("Ingresa otro número: ").trim();
// Se almacenan inputs en un arreglo.
var numbers = [firstNum, secondNum];

// Evalúa que todos los inputs son números.
if (validate(numbers)) {
  // Llama a función que realiza comparación.
  compare(numbers);
}

// Valida inputs.
function validate(numbers) {
  // Función flecha que evalúa si un elemento tiene contenido.
  let checker_empty = (arr) => arr !== "";

  // Se evalúan todos los elementos del arreglo en la función flecha.
  // Condicional se cumple si todos los elementos tienen contenido.
  if (numbers.every(checker_empty)) {
    // Función flecha que evalúa si un elemento no es número.
    let checker_NaN = (arr) => isNaN(Number(arr));

    // Condicional se cumple si alguno de los elementos no es de tipo numérico.
    if (numbers.some(checker_NaN)) {
      alert("¡Error! No se puede realizar la operación.");
    } else {
      // Se retorna true si todos los elementos ingresados son numéricos.
      return true;
    }
  } else {
    alert("¡Error! No se puede realizar la operación.");
  }
}

// Compara números ingresados.
function compare(numbers) {
  // Evalúa si son iguales.
  if (numbers[0] == numbers[1]) {
    alert("Los números son iguales.");
  } else {
    // Número 1 mayor.
    if (numbers[0] > numbers[1]) {
      alert("El número " + numbers[0] + " (primero) es mayor que "
      + numbers[1] + " (segundo).");
    } else {
      // Número 2 mayor.
      alert("El número " + numbers[1] + " (segundo) es mayor que "
      + numbers[0] + " (primero).");
    }
  }
}
